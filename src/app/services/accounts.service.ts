import { Injectable } from '@angular/core';

interface Acc {
  account: number;
  balance: number;
  currency: string;
  lastTransfer: string;
}

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  balanceObj: Acc = {
    account: null,
    balance: null,
    currency: null,
    lastTransfer: null
  };
  userAccounts = [];

  chartData = {
    labels: [],
    data: [],
  };

  accounts = {
    balance: [
      {
        account: 123456789,
        balance: {
          currency: '€',
          value: 765095.54
        },
      owner: 7612333392,
      createdAt: '2012-04-23T18:25:43.511Z'
      },
      {
      account: 987654321,
      balance: {
        currency: '$',
        value: 524323.54
        },
      owner: 7612333392,
      createdAt: '2012-04-23T18:25:43.511Z'
      }
    ]
  };

  transactionsHistory = {
    transactions: [
    {
      fromAccount: 123456789,
      toAccount: 192837465,
      amount: {
        currency: '€',
        value: 876.88
      },
      sentAt: '2012-04-25T20:25:43.511Z'
    },
    {
      fromAccount: 123456789,
      toAccount: 192837465,
      amount: {
        currency: '€',
        value: 654.88
      },
      sentAt: '2012-04-21T18:25:43.511Z'
    },
    {
      fromAccount: 987654321,
      toAccount: 543216789,
      amount: {
        currency: '$',
        value: 543
      },
      sentAt: '2012-04-23T13:25:43.511Z'
    },
    {
      fromAccount: 987654321,
      toAccount: 543216789,
      amount: {
        currency: '$',
        value: 987.54
      },
      sentAt: '2012-04-23T18:25:43.511Z'
    }
    ]
  };

  constructor() {
    this.userAccounts = this.getAccounts();
  }

  /* Get the currency according to the selected account */
  getCurrency(account) {
    return this.accounts.balance.filter(accountItem => accountItem.account === account).map(result => result.balance.currency);
  }

  /* Update transactionsHistory when user tranfers */
  updateTransactions(transfer) {
    this.transactionsHistory.transactions.push(transfer);
    const myAcc = transfer.fromAccount;
    const myAmount = transfer.amount.value;
    const filteredAcc = this.accounts.balance.filter(item => item.account === myAcc)[0];
    filteredAcc.balance.value = filteredAcc.balance.value - myAmount;
  }

  /* Get the user accounts from balance array */
  getAccounts() {
    return this.accounts.balance.map(balance => balance.account);
  }

  getMyBalanceAmount( account ) {
    const acc = +account;
    return this.accounts.balance.find(currAcc => acc === currAcc.account).balance.value;
  }

  /* Get the transactions history */
  getHistory(account: any) {
    return this.transactionsHistory.transactions.filter(transfers => transfers.fromAccount === account);
  }

  /* Get the balance table */
  getBalance() {
    const balance = [];
    this.userAccounts.forEach(element => {
      const transfer = this.getHistory(element)
      .reduce((latest, transfers) => new Date(latest.sentAt) > new Date(transfers.sentAt) ? latest : transfers );
      const accBalance = this.accounts.balance.filter(account => account.account === element).map(account => account.balance);
      const balanceObj: Acc = {
        account: element,
        balance: accBalance[0].value,
        currency: accBalance[0].currency,
        lastTransfer: transfer.sentAt
      };
      balance.push(balanceObj);
    });
    return balance;
  }

  /* Get the required data for charts */
  getTransactionsChart() {
    const labels = [...new Set(this.transactionsHistory.transactions.map((x) => x.toAccount))];
    this.chartData.labels = [];
    this.chartData.data = [];
    this.chartData.labels = [...new Set(this.transactionsHistory.transactions.map((x) => `To Account ${x.toAccount}`))];

    labels.forEach(element => {
      this.chartData.data.push(this.transactionsHistory.transactions.filter((account) => account.toAccount === element)
        .map(account => account.amount.value).reduce((acc, amount) => acc + amount));
    });
  }
}

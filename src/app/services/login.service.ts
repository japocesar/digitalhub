import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements CanActivate {

  isLogged = false;
  username = null;
  page = null;

  constructor( private router: Router ) { }

  logIn() {
    this.isLogged = !this.isLogged;
    this.page = this.isLogged ? '/home' : '/login';
    this.router.navigateByUrl(this.page);
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return this.isLogged;
  }

  /* logOut() {
    this.isLogged =  false;
    this.router.navigateByUrl('/login');
  } */
}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AccountsService } from '../../services/accounts.service';

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss']
})
export class TransferFormComponent implements OnInit {
  errorAmount = false;
  errorMsg = '';
  myBalance = 0;
  userAccounts = null;
  userBalances = [];
  userCurrencies = [];
  account = null;
  amount = null;
  destination = null;

  constructor( private accService: AccountsService ) {
    this.userAccounts = this.accService.userAccounts;
    this.account = this.accService.userAccounts[0];
    this.userBalances = this.accService.getBalance().map(acc => acc.balance);
    this.userCurrencies = this.accService.getBalance().map(acc => acc.currency);
  }

  ngOnInit() {
    this.getBalance();
  }

  getBalance() {
    this.myBalance = +this.accService.getMyBalanceAmount(this.account);
  }

  saveTransfer(f: NgForm) {
    const CURRAMOUNT = +this.amount;
    if ( CURRAMOUNT < 100000 || CURRAMOUNT < this.myBalance ) {
      const myObj = {
        fromAccount: +this.account,
        toAccount: +this.destination,
        amount: {
          currency: this.accService.getCurrency(+this.account),
          value:  CURRAMOUNT,
        },
        sentAt:  new Date().toISOString()
      };
      this.accService.updateTransactions(myObj);
      this.accService.getTransactionsChart();
      this.userBalances = this.accService.getBalance().map(acc => acc.balance);
      this.cancel(f);
    } else {
      this.errorMsg = CURRAMOUNT > 100000 ? 'Transfer must be less than 100,000'
        : 'The amount to transfer is more than the account balance';
      this.errorAmount = true;
    }
  }

  cancel(f: NgForm) {
    this.errorAmount = false;
    f.resetForm();
  }

}

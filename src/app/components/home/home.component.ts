import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { AccountsService } from '../../services/accounts.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public balance = [];

  constructor( public loginservice: LoginService,
               public accountsservice: AccountsService ) { }

  ngOnInit() {
    this.balance = this.accountsservice.getBalance();
  }

}

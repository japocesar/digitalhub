import { Component, OnInit } from '@angular/core';
import { NgForm} from '@angular/forms';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = {
    username: null,
    password: null
  };

  constructor( private loginservice: LoginService) { }

  ngOnInit() {
    const isLogged = false;
  }

  save( form: NgForm) {
    this.loginservice.username = this.user.username;
    this.loginservice.logIn();
  }

}

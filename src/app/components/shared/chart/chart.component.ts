import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, ChartColor } from 'chart.js';
import { MultiLineLabel, SingleDataSet, Color } from 'ng2-charts';
import { AccountsService } from 'src/app/services/accounts.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: false,
    },
  };
  public pieChartLabels: MultiLineLabel[] =  null;
  public pieChartData: SingleDataSet = null;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors: Color[] = [{ backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850']}];

  constructor(public accountsservice: AccountsService) {
  }

  ngOnInit() {
    // const receivedData = this.accountsservice.getTransactionsChart();
    this.accountsservice.getTransactionsChart();
    /* this.pieChartLabels = this.accountsservice.chartData.labels;
    this.pieChartData = this.accountsservice.chartData.data; */
     /*
      pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
      pieChartData: SingleDataSet = [300, 500, 100]
    */
  }

}

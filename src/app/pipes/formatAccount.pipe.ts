import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'formatAccount'})
export class FormatAccount implements PipeTransform {
    transform(value: string): string {
        let newStr: string = '';
        newStr = `*****${value.toString().slice(5)}`;
        return newStr;
    }
}
